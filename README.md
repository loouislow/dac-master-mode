# dac-master-mode

> Set default DAC, set 2 master volume presets when headphone jack in or not

---

## Prerequisites

You need these packages to be installed.

- udev
- sox

Simply type in the Terminal,

```bash
$ sudo apt-get install udev sox
```

## Setup

```bash
cd dac-master-mode
sudo cp dac-master-mode.sh /usr/local/bin/dac-master-mode.sh
sudo mv /usr/local/bin/dac-master-mode.sh dac-master-mode
```
At `cronjob` add these lines,

```bash
@reboot sudo dac-master-mode --install
@reboot sudo dac-master-mode
```

---

## Usage:

> dac-master-mode [-install] [--sleep]

## --install

Create a new master volume preset and set default DAC.

## --sleep
 
Put the preset on halt.

---

## Concept:

I am an audiophiler and I bring my own portable rig to workplace everyday, each time I plug in the USB to use the external high-end Sabre 24bit DAC, most of the time, Ubuntu doesn't choose the right DAC as default, each time I had to set it manually. I would also like to have master volume leveling down to 20-30% when the headphone jacked in, and the master volume return to last setting when the headphone jacked out or disconnected external DAC.
